{-# LANGUAGE OverloadedStrings #-}

import Prelude hiding (null, foldr)
import Control.Applicative()
import Control.Monad
import Control.Concurrent (threadDelay)
import Control.Exception (tryJust)
import System.Environment
import System.Directory
import Data.Aeson
import Data.Aeson.Types (Parser, parseMaybe)
import Data.String (IsString)
import Data.List hiding (null, (\\), foldr)
import Data.Set
import Data.Maybe
import Network.Curl
import Network.Curl.Aeson
import Network.Curl.Opts()
import Sound.ProteaAudio
import qualified Data.Text as T
import Data.Time.Clock
import Data.Time.LocalTime

--TODO
-- Sound on restart? Maybe sound on shutdown? Can I ensure that?

cl :: FromJSON a => String -> Value -> Parser a
cl ep (Object o) = o .: T.pack ep
cl _ _ = mzero

mkCS :: [String] -> String
mkCS = intercalate ","

curltwitch :: String -> String -> String -> IO [Object]
curltwitch endpt reqt argl = do
  ct <- curltwitch' endpt reqt argl
  case ct of
    Left errMsg -> putStrLn errMsg >>
      putStrLn "^^^^^^" >>
      threadDelay 34800000 >>
      curltwitch endpt reqt argl
    Right val -> return val
curltwitch' :: String -> String -> String -> IO (Either String [Object])
curltwitch' endpt reqt argl = tryJust
  nomCAesonExn
  (curlAeson
    (cl endpt)
    "GET"
    ("https://api.twitch.tv/kraken/" ++ endpt ++ "/?" ++ reqt ++ "=" ++ argl)
    [CurlHttpHeaders
      ["Accept: application/vnd.twitchtv.v5+json",
      "Client-ID: l83m9s67f1rbaujdx7lecimuyr7t63"]]
    noData)
  where
    nomCAesonExn :: CurlAesonException -> Maybe String
    nomCAesonExn (CurlAesonException _ _ _ _ errMsg) = Just errMsg

convertNames :: IO ()
convertNames = do
  names <- lines <$> readFile "chanNames"
  let csNames = mkCS names
  result <- curltwitch "users" "login" csNames
  -- Map over, get one ID per line and dump into 'chanIDs'
  let ids = mapMaybe (parseMaybe (.: "_id")) result
  writeFile "chanIDs" (unlines ids)

checkLive :: [String] -> IO (Set String)
checkLive ids = do
  result <- curltwitch "streams" "channel" csIDs
  -- Map over streams listed, channel.display_name ++ " live with " ++ channel.game
  let chans = mapMaybe (parseMaybe (.: "channel")) result
  let strs = getOStr <$> chans
  return $ fromList strs
  where
    csIDs = mkCS ids
    getOStr c = fromJust (parseMaybe (.: "display_name") c) ++ " live with " ++ fromJust (parseMaybe (.: "game") c)

showLive :: Set String -> IO ()
showLive ids = do
  putStrLn $ unlines $ toList ids
  putStrLn "--------------"

getIDs :: IO [String]
getIDs = lines <$> readFile "chanIDs"

getSound :: IO (Maybe Sample)
getSound = do
  exs <- doesFileExist "bloop.wav"
  if exs then do
    retCode <- initAudio 1 44100 1024
    --unless retCode $ fail "Whoopsed up the audio system" --Maybe just go silent? uPIO an error?
    unless retCode $ putStrLn "Whoopsed up the audio system :("
    Just <$> sampleFromFile "bloop.wav" 1
    else return Nothing

bloopIfNew :: Set String -> Maybe Sample -> IO ()
bloopIfNew ids sound =
  unless (null ids) $ do
    now <- getCurrentTime
    timezone <- getCurrentTimeZone
    let (TimeOfDay hour minute _) = localTimeOfDay $ utcToLocalTime timezone now
    let mnt = if minute < 10 then ("0" ++ show minute) else show minute
    let t = "<" ++ show hour ++ ":" ++ mnt ++ ">"
    putStrLn t
--  unless (null ids) $
    maybe (return ()) (\sond -> soundPlay sond 0.1 0.1 0 1) sound

checker :: IO a
checker = do
  ids <- getIDs
  sound <- getSound
  checker' ids empty sound
checker' :: [String] -> Set String -> Maybe Sample -> IO a
checker' ids oldids sound = do
  newids <- checkLive ids
  let diff = newids \\ oldids
  bloopIfNew diff sound
  showLive newids
  threadDelay 69600000
  checker' ids newids sound

{-# ANN argParse ("HLint: ignore Use infix" :: String) #-}
argParse :: (Foldable t, Eq a, Data.String.IsString a) => t a -> IO ()
argParse args
  | elem "-t" args = putStrLn "Test!"
  | elem "-c" args = convertNames >> putStrLn "Converted."
  | otherwise = checker

main :: IO ()
main = do
  args <- getArgs
  argParse args
  finishAudio
